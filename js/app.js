
var inputA = document.getElementById("visorRes");

var historicoDados = [];
var expDados = "";
var resultadosDados = "";


// ---------------- CLICAR NOS BOTÕES COM VALOR ------------------ FUNCIONA


function valor (val) {
	//inputA.value += val;
	var inputVelho = inputA.value;
	var inputNovo = inputVelho + val;

	expDados = inputNovo;

	inputA.value = inputNovo;
}


// ---------------- HISTÓRICO ----------------------------------- FUNCIONA


function showLogdata () {
	var log = document.getElementById("histoLog");
	var strVisor = "";

	for (var key in historicoDados) {
		strVisor +=""+historicoDados[key]["expressao"]+" = "+historicoDados[key]["resultadoHisto"]+"<br>";
	}

	// assim é k mostra na div do histórico!!
	log.innerHTML = strVisor;
}


// ---------------- RESULTADO BOTÃO IGUAL ------------------------ FUNCIONA


// função para apresentar o resultado
function igual (val) {

	//inputA.value = eval(inputA.value);

	var inputHisto = inputA.value;
	var resultadoHisto = eval(inputHisto);

	resultadosDados = resultadoHisto;
	historicoDados.push({"expressao":expDados,"resultadoHisto":resultadosDados});

	showLogdata( );

	resultadosDados = "";
	expDados = "";

	//Apagar nºs no fim do cálculo
	inputA.value = resultadoHisto;
	
	// para apagar o ecrã, mas o resultado só aparece no histórico e não no visor
	//inputA.value = null;

	// assim também mostra no console.log
	console.log(inputHisto);
	console.log(resultadoHisto);
	console.log(resultadosDados);
}


// ---------------- CIENTÍFICA ----------------------------------- FUNCIONA


// variável da flag
	var a = 0;

function mostrar () {

	var esconde = document.getElementById("cientifica");

    if (a == '0')
    {
        esconde.style.display = 'block';
        document.getElementById('FundoCor').style.minHeight = '400px';
        document.getElementById('btnMostra').innerHTML = "Normal";

        /* para divHisto ter o mesmo tamanho k a div da cientifica */
        document.getElementById('divHistoTamanho').style.minHeight = '430px';

        a = 1;
    }
    else
    {
    	document.getElementById('FundoCor').style.minHeight = '300px';
    	document.getElementById('btnMostra').innerHTML = "Cientifica";
    	esconde.style.display = 'none';

    	/* para divHisto voltar a ter o mesmo tamanho k antes da cientifica */
    	document.getElementById('divHistoTamanho').style.minHeight = '365px';

    	a = 0;
	}
}


// ---------------- MUDAR COR DE FUNDO ALEATÓRIA -------------------- FUNCIONA


var a = 0;

function BackgroundColor() {
	
	if(a==0){
		document.getElementById('FundoCor').style.backgroundColor = '#C25B5F';
		a=1; // flag passa a ser um, ficando vermelho
	}
	else if (a==1) {
		document.getElementById('FundoCor').style.backgroundColor = '#7BA5B7';
		a=2; // flag passa a ser um, ficando azul
	}
	else if (a==2) {
		document.getElementById('FundoCor').style.backgroundColor = '#9CB36B';
		a=3; // flag passa a ser um, ficando verde
	}
	else if (a==3) {
		document.getElementById('FundoCor').style.backgroundColor = 'lightgrey';
		a=0; // flag passa a ser um, ficando cinzento claro
	}
}


// ----------------  IMPRIMIR -------------------------------------- FUNCIONA


function imprimir(){
	window.print();
}


// ---------------- BOTÃO RECARREGAR ------------------------------- FUNCIONA


function recarregar () {
	window.location.reload();
}


// ---------------- BOTÃO ACERCA ----------------------------------- FUNCIONA


function acerca () {
	// aparece o alerta
	alert("Calculadora desenvolvida por: \n Ana Margarida Costa  |  2021");
}


// ---------------- APAGAR VALORES UM A UM --------------------------- FUNCIONA


// função para apagar o último valor inserido
function apagarUm (val) {

	var c = inputA.value;
	var apaga = c.substring(0, c.length-1);
	inputA.value = apaga;
}


// ---------------- APAGAR VALORES - C ------------------------------ FUNCIONA


function apagarC (val) {
	inputA.value = "";
}


// ---------------- RAIZ QUADRADA ------------------------------------ FUNCIONA


function raizquadrada(){
	var d = inputA.value;
    var resRaiz = Math.sqrt(d);

    inputA.value = resRaiz;
}


// ---------------- RAIZ POTÊNCIA -------------------------------------- FUNCIONA


function potencia (val) {
	var a = inputA.value;
	var b = prompt("Digite o valor do expoente que deseja calcular");
    inputA.value = Math.pow(a,b);
}


// ---------------- RAIZ PERCENTAGEM ----------------------------------- FUNCIONA


function percentagem (perct) {
	//função para calcular a percentagem
	var pergunta = prompt("Digite o segundo valor da percentagem que deseja calcular");

   	inputA.value +="*"+(pergunta/100);

   	console.log(pergunta);
   	console.log(inputA);

	//inputA.value *= 0.01;
	//inputA.value = inputA.value +="*"+(inputA.value/100);
}
